package ru.hnau.remote_teaching_common.utils

import ru.hnau.jutils.TimeValue
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.data.test.TestTask
import ru.hnau.remote_teaching_common.data.test.TestTaskType
import ru.hnau.remote_teaching_common.data.test.TestTaskVariant
import ru.hnau.remote_teaching_common.exception.ApiException


object Validators {

    private val BASE_SUPPORTED_SYMBOLS = (('а'..'я') + ('А'..'Я') + ('a'..'z') + ('A'..'Z') + ('0'..'9') + "!@#$%^&*()_+=-;:,.<>{}[]/?№".toList()).toHashSet()
    private val BASE_SUPPORTED_SYMBOLS_WITH_SPACE = BASE_SUPPORTED_SYMBOLS + ' '

    const val MIN_LOGIN_LENGTH = 3
    const val MAX_LOGIN_LENGTH = 256
    val SUPPORTED_LOGIN_SYMBOLS = BASE_SUPPORTED_SYMBOLS

    fun validateUserLoginOrThrow(login: String) =
            validateString(login, "логина", MIN_LOGIN_LENGTH, MAX_LOGIN_LENGTH, SUPPORTED_LOGIN_SYMBOLS)


    const val MIN_PASSWORD_LENGTH = 3
    const val MAX_PASSWORD_LENGTH = 256
    val SUPPORTED_PASSWORD_SYMBOLS = BASE_SUPPORTED_SYMBOLS

    fun validateUserPasswordOrThrow(password: String) =
            validateString(password, "пароля", MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH, SUPPORTED_PASSWORD_SYMBOLS)


    const val MIN_NAME_LENGTH = 3
    const val MAX_NAME_LENGTH = 256
    val SUPPORTED_NAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateUserNameOrThrow(name: String) =
            validateString(name, "имени", MIN_NAME_LENGTH, MAX_NAME_LENGTH, SUPPORTED_NAME_SYMBOLS)


    const val MIN_SURNAME_LENGTH = 3
    const val MAX_SURNAME_LENGTH = 256
    val SUPPORTED_SURNAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateUserSurnameOrThrow(surname: String) =
            validateString(surname, "фамилии", MIN_SURNAME_LENGTH, MAX_SURNAME_LENGTH, SUPPORTED_SURNAME_SYMBOLS)


    const val MIN_PATRONYMIC_LENGTH = 3
    const val MAX_PATRONYMIC_LENGTH = 256
    val SUPPORTED_PATRONYMIC_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateUserPatronymicOrThrow(patronymic: String) =
            validateString(patronymic, "отчества", MIN_PATRONYMIC_LENGTH, MAX_PATRONYMIC_LENGTH, SUPPORTED_PATRONYMIC_SYMBOLS)


    const val MIN_STUDENTS_GROUP_NAME_LENGTH = 3
    const val MAX_STUDENTS_GROUP_NAME_LENGTH = 256
    val SUPPORTED_STUDENTS_GROUP_NAME_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateStudentsGroupNameOrThrow(studentsGroupName: String) =
            validateString(studentsGroupName, "названия группы", MIN_STUDENTS_GROUP_NAME_LENGTH, MAX_STUDENTS_GROUP_NAME_LENGTH, SUPPORTED_STUDENTS_GROUP_NAME_SYMBOLS)


    const val MIN_TEST_TITLE_LENGTH = 3
    const val MAX_TEST_TITLE_LENGTH = 256
    val SUPPORTED_TEST_TITLE_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateTestTitleOrThrow(testTitle: String) =
            validateString(testTitle, "названия теста", MIN_TEST_TITLE_LENGTH, MAX_TEST_TITLE_LENGTH, SUPPORTED_TEST_TITLE_SYMBOLS)

    fun validateTestPassPercentageOrThrow(testPassPercentage: Float) {
        if (testPassPercentage <= 0) {
            throw ApiException.raw("Проходной балл должен быть больше 0%")
        }
        if (testPassPercentage > 1) {
            throw ApiException.raw("Проходной балл должен быть меньше или равен 100%")
        }
    }

    fun validateTestTasksOrThrow(testTasks: List<TestTask>) =
            testTasks.forEach(this::validateTestTaskOrThrow)

    fun validateTestTaskOrThrow(testTask: TestTask) {
        validateTestTaskMaxScoreOrThrow(testTask.maxScore)
        testTask.variants.forEach { variant ->
            validateTestTaskVariantOrThrow(variant, testTask.type)
        }
    }

    fun validateTestTaskMaxScoreOrThrow(testTaskMaxScore: Int) {
        if (testTaskMaxScore <= 0) {
            throw ApiException.raw("Максимальный балл задания должен быть больше 0")
        }
    }

    fun validateTestTaskVariantOrThrow(testTaskVariant: TestTaskVariant, taskType: TestTaskType) {
        validateTestTaskVariantDescriptionMDOrThrow(testTaskVariant.descriptionMD)
        if (testTaskVariant.optionsMD.size < 2 && taskType != TestTaskType.TEXT) {
            throw ApiException.raw("Вариант задания должен иметь несколько вариантов ответа")
        }
        testTaskVariant.optionsMD.forEach(this::validateTestTaskVariantOptionMDOrThrow)
        validateTestTaskVariantResponseOrThrow(testTaskVariant.responseParts, testTaskVariant.optionsMD, taskType)
    }

    fun validateTestTaskVariantResponseOrThrow(
            responseParts: List<String>,
            optionsMD: List<String>,
            taskType: TestTaskType
    ) {

        if (taskType != TestTaskType.MULTI && responseParts.isEmpty()) {
            throw ApiException.raw("Ответ не может быть пустым")
        }

        if (taskType == TestTaskType.TEXT) {
            responseParts.forEach {
                if (it.isEmpty()) {
                    throw ApiException.raw("Вариант ответа не может быть пустым")
                }
            }
            return
        }

        val responseOptions = responseParts.map(String::toInt)
        if (taskType == TestTaskType.SINGLE && responseOptions.size != 1) {
            throw ApiException.raw("Для задания с одним ответом ответ может быть только один")
        }

        val optionsCount = optionsMD.size
        responseOptions.forEach { responseOptionNumber ->
            if (responseOptionNumber < 0 || responseOptionNumber >= optionsCount) {
                throw ApiException.raw("Неверный номер ответа $responseOptionNumber")
            }
        }

    }

    const val MIN_TEST_TASK_VARIANT_DESCRIPTION_MD_LENGTH = 3
    const val MAX_TEST_TASK_VARIANT_DESCRIPTION_MD_LENGTH = 1024
    val SUPPORTED_TEST_TASK_VARIANT_DESCRIPTION_MD_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateTestTaskVariantDescriptionMDOrThrow(testTaskVariantDescriptionMD: String) =
            validateString(testTaskVariantDescriptionMD, "описания варианта задания", MIN_TEST_TASK_VARIANT_DESCRIPTION_MD_LENGTH, MAX_TEST_TASK_VARIANT_DESCRIPTION_MD_LENGTH, SUPPORTED_TEST_TASK_VARIANT_DESCRIPTION_MD_SYMBOLS)

    const val MIN_TEST_TASK_VARIANT_OPTION_MD_LENGTH = 3
    const val MAX_TEST_TASK_VARIANT_OPTION_MD_LENGTH = 256
    val SUPPORTED_TEST_TASK_VARIANT_OPTION_MD_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateTestTaskVariantOptionMDOrThrow(testTaskVariantOptionMD: String) =
            validateString(testTaskVariantOptionMD, "варианта ответа", MIN_TEST_TASK_VARIANT_OPTION_MD_LENGTH, MAX_TEST_TASK_VARIANT_OPTION_MD_LENGTH, SUPPORTED_TEST_TASK_VARIANT_OPTION_MD_SYMBOLS)


    val TEST_MIN_TIME_LIMIT = TimeValue.MINUTE
    val TEST_MAX_TIME_LIMIT = TimeValue.DAY * 10
    fun validateTestTimeLimitOrThrow(timeLimit: Long) {
        if (timeLimit <= TEST_MIN_TIME_LIMIT.milliseconds) {
            throw ApiException.raw("Время на выполнение теста должно быть больше или равно чем ${TEST_MIN_TIME_LIMIT.uiString}")
        }
        if (timeLimit > TEST_MAX_TIME_LIMIT.milliseconds) {
            throw ApiException.raw("Время на выполнение теста должно быть меньше или равно чем ${TEST_MAX_TIME_LIMIT.uiString}")
        }
    }

    const val MIN_SECTION_TITLE_LENGTH = 3
    const val MAX_SECTION_TITLE_LENGTH = 256
    val SUPPORTED_SECTION_TITLE_SYMBOLS = BASE_SUPPORTED_SYMBOLS_WITH_SPACE

    fun validateSectionTitleOrThrow(sectionTitle: String) =
            validateString(sectionTitle, "названия раздела", MIN_SECTION_TITLE_LENGTH, MAX_SECTION_TITLE_LENGTH, SUPPORTED_SECTION_TITLE_SYMBOLS)

    fun validateActionCodeOrThrow(actionCode: String, actionCodeType: ActionCodeType) {
        if (actionCode.length != actionCodeType.codeLength) {
            throw ApiException.raw("Длина кода (${actionCode.length}) некорректна. Ожидаемая длина - ${actionCodeType.codeLength}")
        }
        actionCode.forEach {
            if (it !in ActionCodeUtils.SYMBOLS) {
                throw ApiException.raw("Недопустимый символ в коде - '$it'")
            }
        }
    }

    private fun validateString(
            string: String,
            name: String,
            minLength: Int,
            maxLength: Int,
            supportedSymbols: Set<Char>
    ) {
        if (string.length < minLength) {
            throw ApiException.raw("Минимальная длина $name - $minLength")
        }

        if (string.length > maxLength) {
            throw ApiException.raw("Максимальная длина $name - $maxLength")
        }

        string.forEach {
            if (it !in supportedSymbols) {
                throw ApiException.raw("Недопустимый символ в $name - '$it'")
            }
        }


    }

}