package ru.hnau.remote_teaching_common.exception


sealed class ApiExceptionContent {

    open class Common(
            val message: String
    ) : ApiExceptionContent() {

        override fun generateDescription() =
                "message=$message"

    }

    object Undefined : Common("Неизвестная ошибка")

    object Authentication : ApiExceptionContent()

    object SectionHasChildren : ApiExceptionContent()

    object UserWithLoginAlreadyExists : ApiExceptionContent()

    class UnsupportedVersion(
            val oldVersion: Int,
            val newVersion: Int,
            val comment: String,
            val updateUrl: String?
    ) : ApiExceptionContent() {

        override fun generateDescription() =
                listOfNotNull(
                        "oldVersion=$oldVersion",
                        "newVersion=$newVersion",
                        "comment=$comment",
                        updateUrl?.let { "updateUrl=$it" }
                ).joinToString(",")

    }

    open class DdosBlocked(
            val secondsToUnlock: Long?
    ) : ApiExceptionContent() {

        override fun generateDescription() =
                "secondsToUnlock=$secondsToUnlock"

    }

    class DdosBlockedIp(
            secondsToUnlock: Long?
    ) : DdosBlocked(secondsToUnlock)

    class DdosBlockedUser(
            secondsToUnlock: Long?
    ) : DdosBlocked(secondsToUnlock)

    object IncorrectActionCode : ApiExceptionContent()

    object AdminPasswordNotConfigured : ApiExceptionContent()

    object HostNotConfigured : ApiExceptionContent()

    object Network : Common("Ошибка связи с сервером")

    protected open fun generateDescription(): String? = null

    override fun toString() =
            "${javaClass.simpleName}${generateDescription()?.let { "($it)" } ?: ""}"

}