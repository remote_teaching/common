package ru.hnau.remote_teaching_common.data


enum class UserPermission(
        val title: String
) {

    MANAGE_STUDENTS("Управление студентами"),
    MANAGE_TEACHERS("Управление преподавателями"),
    MANAGE_SECTIONS("Управление разделами"),
    MANAGE_GROUPS("Управление группами")

}