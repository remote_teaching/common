package ru.hnau.remote_teaching_common.data.notification


data class RTToUserNotification(
        val toUser: String,
        val notification: RTNotification.Serialized
) {

    companion object {

        const val SERIALIZATION_KEY_TO_USER = "toUser"
        const val SERIALIZATION_KEY_CLASS = "class"
        const val SERIALIZATION_KEY_CONTENT = "content"

    }

}