package ru.hnau.remote_teaching_common.data.notification

import ru.hnau.remote_teaching_common.utils.GSON


abstract class RTNotification {

    data class Serialized(
            val className: String,
            val json: String
    )

    companion object {

        private const val NOTIFICATION_CLASS_NAME_PREFIX = "ru.hnau.remote_teaching_common.data.notification.type"

        fun deserialize(
                className: String,
                json: String
        ): RTNotification {
            val contentClassName = "$NOTIFICATION_CLASS_NAME_PREFIX.$className"
            val contentClass = Class.forName(contentClassName)
            return GSON.fromJson(json, contentClass) as RTNotification
        }

    }

    fun serialize() = Serialized(
            className = javaClass.simpleName,
            json = GSON.toJson(this)
    )

    protected open fun generateDescription(): String? = null

    override fun toString() =
            "${javaClass.simpleName}${generateDescription()?.let { "($it)" } ?: ""}"

}