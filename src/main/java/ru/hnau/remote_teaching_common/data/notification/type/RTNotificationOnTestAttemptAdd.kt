package ru.hnau.remote_teaching_common.data.notification.type

import ru.hnau.remote_teaching_common.data.notification.RTNotification
import ru.hnau.remote_teaching_common.data.test.attempt.TestAttempt


class RTNotificationOnTestAttemptAdd(
        val testAttempt: TestAttempt
) : RTNotification() {

    override fun generateDescription() =
            "testAttempt=$testAttempt"

}