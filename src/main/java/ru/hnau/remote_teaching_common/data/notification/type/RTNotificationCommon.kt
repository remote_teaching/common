package ru.hnau.remote_teaching_common.data.notification.type

import ru.hnau.remote_teaching_common.data.notification.RTNotification


class RTNotificationCommon(
        val message: String
) : RTNotification() {

    override fun generateDescription() =
            "message=$message"

}