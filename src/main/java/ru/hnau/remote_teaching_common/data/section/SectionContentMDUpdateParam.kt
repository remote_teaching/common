package ru.hnau.remote_teaching_common.data.section


data class SectionContentMDUpdateParam(
        val contentMD: String
)