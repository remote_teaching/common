package ru.hnau.remote_teaching_common.data.section

import ru.hnau.remote_teaching_common.data.test.TestSkeleton


data class SectionInfo(
        val subsections: List<SectionSkeleton> = emptyList(),
        val tests: List<TestSkeleton> = emptyList(),
        val contentMD: String = ""
)