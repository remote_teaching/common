package ru.hnau.remote_teaching_common.data.test


data class TestTask(
        val maxScore: Int = 1,
        val type: TestTaskType = TestTaskType.SINGLE,
        val variants: List<TestTaskVariant> = emptyList()
) {

    companion object

}