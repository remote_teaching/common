package ru.hnau.remote_teaching_common.data.test

import ru.hnau.jutils.TimeValue


data class TestSkeleton(
        val uuid: String = "",
        val sectionUUID: String = "",
        val title: String = "",
        val passScorePercentage: Float = DEFAULT_PASS_SCORE_PERCENTAGE,
        val timeLimit: Long = DEFAULT_TIME_LIMIT
) {

    companion object {

        val DEFAULT_TIME_LIMIT = TimeValue.HOUR.milliseconds
        const val DEFAULT_PASS_SCORE_PERCENTAGE = 0.5f

    }

}