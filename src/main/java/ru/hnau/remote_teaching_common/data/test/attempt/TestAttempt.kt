package ru.hnau.remote_teaching_common.data.test.attempt


data class TestAttempt(
        val uuid: String = "",
        val testUUID: String = "",
        val testTitle: String = "",
        val studentsGroupName: String = "",
        val timeLeft: Long = 0L
) {

    companion object

}