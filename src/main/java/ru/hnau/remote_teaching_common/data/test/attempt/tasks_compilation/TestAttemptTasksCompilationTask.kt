package ru.hnau.remote_teaching_common.data.test.attempt.tasks_compilation

import ru.hnau.remote_teaching_common.data.test.TestTaskType
import ru.hnau.remote_teaching_common.data.test.TestTaskVariant


data class TestAttemptTasksCompilationTask(
        val maxScore: Int = 1,
        val type: TestTaskType = TestTaskType.SINGLE,
        val variant: TestTaskVariant = TestTaskVariant()
)
