package ru.hnau.remote_teaching_common.data.test.attempt.tasks_compilation

import ru.hnau.remote_teaching_common.data.test.TestSkeleton


data class TestAttemptTasksCompilation(
        val test: TestSkeleton = TestSkeleton(),
        val tasks: List<TestAttemptTasksCompilationTask> = emptyList()
) {

    companion object

}