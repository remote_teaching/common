package ru.hnau.remote_teaching_common.data.test.attempt


data class StudentsGroupTestResults(
        val results: Map<String, Float> = emptyMap()
)