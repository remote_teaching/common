package ru.hnau.remote_teaching_common.data.test

import ru.hnau.jutils.takeIfNotEmpty


data class TestTaskVariant(
        val descriptionMD: String = "",
        val optionsMD: List<String> = emptyList(),
        val responseParts: List<String> = emptyList()
)