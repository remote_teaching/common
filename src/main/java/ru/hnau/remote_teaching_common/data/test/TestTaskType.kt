package ru.hnau.remote_teaching_common.data.test


enum class TestTaskType {
    SINGLE,
    MULTI,
    TEXT
}