package ru.hnau.remote_teaching_common.data


data class User(
        val login: String = "",
        val name: String = "",
        val surname: String = "",
        val patronymic: String = "",
        val studentsGroupName: String = "",
        val role: UserRole = UserRole.STUDENT
) {

    companion object {
        const val ADMIN_LOGIN = "admin"
        const val DEFAULT_ADMIN_PASSWORD = "password"
    }

}