package ru.hnau.remote_teaching_common.api


object ApiUtils {

    const val HEADER_CLIENT_TYPE = "Client-Type"
    const val HEADER_CLIENT_VERSION = "Client-Version"
    const val HEADER_AUTH_TOKEN = "Auth-Token"

}